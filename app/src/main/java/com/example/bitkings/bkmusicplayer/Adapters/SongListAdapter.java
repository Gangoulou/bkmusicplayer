package com.example.bitkings.bkmusicplayer.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.bitkings.bkmusicplayer.R;
import com.example.bitkings.bkmusicplayer.DataHolders.Song;

import java.util.ArrayList;

public class SongListAdapter extends RecyclerView.Adapter<SongListAdapter.ItemViewHolder> {

    private ArrayList<Song> songList = new ArrayList<>();

    final private ListItemClickListener mListItemClickListener;

    public interface ListItemClickListener{
        void OnListItemClick(String songName, String clickedItemIndex);
    }

    public SongListAdapter(ArrayList<Song> songList, ListItemClickListener listener)
    {
        this.songList = songList;
        mListItemClickListener = listener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.song_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListItem,parent,false);
        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(position,songList);
    }

    @Override
    public int getItemCount() {
        return songList.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView songAuthor;
        TextView songName;
      ItemViewHolder(View itemView) {
            super(itemView);
            songAuthor = (TextView) itemView.findViewById(R.id.tv_song_author);
            songName = (TextView) itemView.findViewById(R.id.tv_song_name);
            Button play = (Button) itemView.findViewById(R.id.button);
            play.setOnClickListener(this);
        }

        void bind(int listIndex, ArrayList<Song> songList) {
            Song song = songList.get(listIndex);
            songAuthor.setText(song.author);
            songName.setText(song.name);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            Song song = songList.get(clickedPosition);
            String songName = song.name;
            String songUri = song.uri;
            mListItemClickListener.OnListItemClick(songName, songUri);
        }
    }
}