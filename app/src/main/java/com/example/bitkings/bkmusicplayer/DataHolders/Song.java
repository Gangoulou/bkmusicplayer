package com.example.bitkings.bkmusicplayer.DataHolders;

public class Song {

    public String name;
    public String author;
    public String uri;


    public Song(String name, String author,String url) {
        this.name = name;
        this.author = author;
        this.uri = url;
    }
}